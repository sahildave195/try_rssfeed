package com.example.try_rssfeed;

import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

public class MainActivity extends ListActivity {

	private static final String TAG = "RSSFEED"; // For Logcat

	// ArrayList<String> items = new ArrayList<String>();

	@Override
	public void onCreate(Bundle icicle) {

		super.onCreate(icicle);

		Log.d(TAG, "onCreate");

		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new ArrayList<String>()));
		new MyAsyncTask().execute();

	}

	private class MyAsyncTask extends AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... unused) {

			Log.d(TAG, "inBackground");

			int count = 1; // For Logcat
			try {

				XmlPullParser xpp = getResources().getXml(R.xml.toi2);

				do {
					Log.d(TAG, "in While "+count);
					if (xpp.getEventType() == XmlPullParser.START_TAG && xpp.getName().equals("title")) {
						xpp.next();
						Log.d(TAG, xpp.getEventType() + "____" + count++);
						publishProgress(xpp.getText());
					}
					xpp.next();
				} while (xpp.getEventType() != XmlPullParser.END_DOCUMENT);

			} catch (Throwable t) {

			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			((ArrayAdapter<String>) getListAdapter()).add(values[0]);
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.d(TAG, "===================");
			Log.d(TAG, "DONE DONE DONE DONE");
		}

	}

}
